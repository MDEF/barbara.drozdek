---
title: 01. MDEF Bootcamp
period: 1-7 October 2018
date: 2018-10-07
term: I
published: true
---


***	Who are you?***

This was the first question I heard on the first day at the MDEF in IAAC. I think it was a good start for me to get to know not only myself and other members of the group as well. I don't feel like an industrial designer but officially I am. Maybe I'm a process designer, but who knows what it means? These are only categories that don't describe me as a human in the world in which we are overwhelmed by the excess of information. I hope that for the next month I may not find my place, but I will find the path that I will want to follow.


***	Why are you here?***

I have been wondering for several years, what is the role of the designer these days? As the realms of design and manufacturing converge and the designer increasingly takes on the role of both inventor and maker, production is subject to change. Creatives deconstruct and disrupt, reverse and reinvent: they seek to shake up the status quo by exploring previously undiscovered paths in the social and technological processes. However, I believe designers should take design one step further. They should focus their attention on the processes, which influence our environment.


***	How do you see yourself in 10 years?***

I will live in a house in a forest in the south of Poland. The utopian vision of my future, which is a kind of escape from the city. Of the cities in which 70% of our population will live. It will be an attempt to build a small supporting community. The question which is very difficult for me as well is: Who is your guru?  One wise man said that his guru is himself in 10 years. Then I realized that with me is the same. There is no one person who is my guru, there is no one place where I am going to live, there is no idea what I am going to do. The only thing I know now is that I want to improve myself.

# The city is our tool

**During the bootcamp week, I have learned and refreshed various types of skills which will be helpful to explore the city, to understand the citizen's needs and find the answer to the question about the role of the designers in this process.**

![]({{site.baseurl}}/poblenou.jpg)
*Poblenou district*


- Documentation - while I was writing the first post I discovered how important is the documentation of the process, collecting and classification of information, thoughts, pictures. How to your own way of systematization your work?

- Teamwork - as a team, we speak with one voice, as members of the team, we also have our opinions. The fact that we have a different experience that we come from other backgrounds and cultures forces us to change our point of view and to understand the other person's options. This stimulates our imagination but also encourages constructive criticism.

- City - our city/ our neighbourhood is our tool. We should know what is a dynamic of the city. I discovered that the city is built of layers. The layers that overlap but don't form one whole. The question is who or what is a binder/bridge between them? Citizens? If there are citizens, who live in our district? Technology? If there is technology, how to use technology to change a city? What is the relationship between citizens and technology? Is digital fabrication available to citizens? After the exploration tour in Poblenou (as a city prototype), there were many questions that remain unanswered for now. It means the potential is huge and we have to start working.

![]({{site.baseurl}}/working space.jpg)
*working spaces in @22*

- Material - the sources of obtaining material change. What means raw materials nowadays? What can be raw? What is raw? What doesn't exist any more like a raw material? Maybe we should start with redefining the meaning of words/definition because in a specific context, localization, society it reflects differently.

- Data  - relationship and data exchange between nature, human and machine. What is natural and what is artificial and how does it reflect on our lives?

![]({{site.baseurl}}/artificial natural.jpg)
*artificial/ natural*

---
title: 04. Exploring Hybrid Profiles in Design
period: 22-28 October 2018
date: 2018-10-28 12:00:00
term: 1
published: true
---

# What your skills/ knowledge/ attitude tell about you?

**Exploring the hybrid profiles in design by visiting different design studios gave us the references points to start thinking about our identity.**

**Domestic data streamers** is a multidisciplinary team of about  30 people trying to translate abstract data to the story.

What I like…?
That in this studio people know how to deal with and understand that abstract data needs storytelling. Storytelling is a very useful tool, useful nowadays. Every object tells a story but I noticed that in our still consumption world the customer pays attention to how the object  was made and what kind of story is behind this object? Is this a new/old value that has always been a hidden message before the client will give a new object valuation system?

The way how they use technology as a tool to trigger people to interact with a technology and get a data as a base for a service design.

The different type of ethnographic techniques and different kind of research methodology that they use to analyze and dissect the data.

In that case I noticed that working in multidisciplinary team in a combination of different fields of design (product, communication, industrial, music, interior etc. designer) and social sciences (anthropologist, sociologist, philosopher, psychologist) makes the project gaining value through the dialogue of various points of view on the problem. This collaboration has a good impact on the project, but also on the people involved in the project, which learn from each other.

I really like the idea of open design first time I heard about this on a Richard Sennett talk - Lets users adapt the it. The involvement of people in the project has a great driving force. I believe in that, and in the same time it goes with customization consistent with their needs.

The knowledge about how humans interact in a specific situation is super interesting. Can we use our body as a tool? How people from different professional/cultural background react to external stimuli?

I thought that talking about the responsibility in design exist only in design schools and after graduation most of design student don't care about that because the clash with real world  after leaving the bubble of the design environmental, overwhelms many of them.

Design does and design do. The domestic data streamers is transparent in what they doing.
There is no hidden information behind that.

***It's a process!***

**Exercise:** We did an exercise which showed us that data can be very powerful and by analyzing it, we can learn about ourselves the things we were not even aware of before. Analyzing what things we carry with us everyday and answering questions such as: Why I am doing this? What kind of history / information they bring with each other these objects in a specific configuration.

<photo>

**Sara de Ubierta** is a designer with architectural background. In her work she is balancing between product design and art.

What I like…?
That she is very engage in a craft and old craft techniques. She has a huge respect for a craftsmen work and knowledge. She started with a research about: How to become a master? It's very interesting nowadays when we can observe that some of the old profession are disappearing like furrier, brushmaker, bookbinder. Will the knowledge of disappearing professions survive? How to keep this knowledge? How to keep this knowledge without losing the human factor? Can we somehow keep the metis knowledge based processes? How?

Material research and curiosity about the new materials has never been in this context of shoemaking. Analyze the properties on the material physical and chemical and catching the most interesting and characteristic parameters to check them. How to control the material?

Analog way of working drive the process of production with new  materials very limited products. What means the product is not available for everyone?  Why was struggling during the visit and after it. Why she doesn't use a technology to develop her projects and process of making  to be more precise.

The knowledge which she get from the master of shoemaking she is trying to translate for a new materials implication by rethinking about the process of production through the material. But still based of a knowledge about shoemakers.

Material focused design, the process of creation from the beginning based on a material. The material as a driver in creation plus human curiosity

I like that she is aware that she works  on the edges of old industries model produce. The awareness  how that system works that the factories produce but does not research did not break her core. what's more she is trying to reframe the process of making for more sustainable to make it circular and what's influence her about material design decision.

***I do something for design and it is not a product.***

**Exercise:** We did a shoe. Our starting point was the material reused pressed cotton. The task focused on that: How we work with it? How we learn from it by making and trying? My group had a lot of fun doing this.  I am not sure it was the new material excitement, involvement of people in the team or just work in a physical world.


<picture>

**Ariel Guersenzvaig**
That was very impressive that he works in four field of design research/ managing/ teaching/ designing  in the same time since twenty years.  The flexibility which he has the wide spectrum of possibilities it helped him to survive and to overcome professional and private crises and successes in his life. The choice is a privilege that we should ensure ourselves.

To be aware of our competences and be able to define where in the hierarchy of competence are we. Are we on a beginning of the path? We are beginner and we are follow all of procedures and checklist that guide us how start.  Or we feels more comfortable and we are able to play with the primary problem definition: generate solution by defining, framing, reframing and redefining. The design decision making strongly connected with design/humanities, ethical intuition (wrong/right) and human behavior. It shows in social activities:  as an internal goods that you can achieve through (dancing, playing chasse) or an external goods capability approach potential/ political philosophical frame. Professional designer decision is between them both.

To understand the present and the future better the knowledge about the past is necessary. The information architecture about past/present/future should be driven by our curiosity. Curiosity should be our personal drive to act/work/do.

Design is a profession. It is not about new products on the shop window that drive global consumerism. Design is for increasing people capabilities. This attitude and research for/through design  lets us still believe that as a animal human we are aware of the consequence of action.

***The things come in good time and place, always.***

**Who I am?**

![]({{site.baseurl}}/WHO I AM.png)

[link](http://arborjs.org/halfviz/#/MTIyNzc)


**Who would you like to be?** personal plan development

![]({{site.baseurl}}/SKILLS.jpg)

[skills link](http://arborjs.org/halfviz/#/MTIyNzU)

![]({{site.baseurl}}/knowlegde.png)

[knowledge link](http://arborjs.org/halfviz/#/MTIyNzM)

![]({{site.baseurl}}/ATTITUDE.png)

[attitude link](http://arborjs.org/halfviz/#/MTIyNzg)

**What do you want to learn during the first term?** **What will help you improve your idea?**

| MDEF   |      DESCRIPTION     |  HOW CAN IT HELP ME TO IMPROVE MYSELF |
|----------|:-------------:|------:|
| Bootcamp |    x   |   x |
| Biology zero |    biotechnology   |   knowledge about new materials and biology processes, scientific methodology, critical thinking, cognitive flexibility |
| Design for real digital world | introduction to digital fabrication tools |  open design, linking economy-society-ecology   |
| Exploring the hybrid profiles in design |   hybrid design profiles   |   flexibility in working in design  |
| Navigating the Uncertainty | framing research questions in an age massive challenges |   cognitive flexibility, problem defining |
| Designing with Extended Intelligence |    from the neolithic to post-humans  |   cognitive flexibility, questioning myself, critical thinking |
| The way things work | an introduction to physical computing by hacking everyday objects |    programming materials, open design, new materials implication |
| Living with ideas |    temporal aspects of embodied living with prototypes  |  human behavior/interaction  |
| Engaging Narratives | research focused, narratives workshop with Kickstarter |  research methodology and techniques   |
| An Introduction to Futures |    a week in the future   |   linking economy-politic-society-ecology, storytelling |
| From Bits to Atoms | pre-fab academy course |  knowledge about processes |
| Design Dialogues |    x  |   x |


**How to get a sense of** (personal) **direction?**

The personal driver (pushing force) to start doing. Be open minded and sensitive ready to redefine, reframe, frame and solve or transform the complex problem. In the same time be flexibility and do not be imposing our vision. On a personal experience we build our personal identity (tool). It is a tool and methods to reflect yourself. How we are do this? It is up to as the strategy it can be an annotation, exemplar, guideline. For me, the most difficult is self-criticism and self-inquiry - why? This is one of the goals to achieve in the coming term. The combination of experience and reflection translates into our vision.  Vision of us in the future as a designer based on a learning from mistakes, gathering data, openness and flexibility. Building a vision and speculate about our future and what kind of impact on society brings our action.

---


{% figure caption: "*Markdown* caption" %}
![]({{site.baseurl}}/transfolab.jpeg)
{% endfigure %}

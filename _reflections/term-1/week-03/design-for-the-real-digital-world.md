---
title: 03. Design for the Real Digital World
period: 15-21 October 2018
date: 2018-10-21 12:00:00
term: 1
published: true
---

# From "new" raw materials to the final product

**The Anthropocene is a geological time when the changes on earth stem primarily from human activities. All our actions can really have radical consequences for the world. There is no alternative in the form of planet B.**

![]({{site.baseurl}}/rdw2.jpg)

But it is not about doing less, on the contrary, it is about doing more. That is what the classes *Design for the real digital world* led by Ingi Freyr Gudjonsson were about.  During the whole week, we were trying to answer the question: How can digital fabrication tools be used in creative ways to work with the "new" raw materials? What is the "new" raw material? It is the scrap/trash sourced from the streets.  Last year Daan Roosegaarde said in his TEDtalk :

*The waste should not exist, the waste for the one should be food for the other.*

I agree with him. This is why I believe those objects are the "new" raw material in the Anthropocene era. This does not mean he has discovered a solution for the global waste problem. This is a path that is supposed to prevent the production of waste by reducing consumption. It means that when we, the designers, are designing new objects, we should especially define what will be the second life of the object after it has been used. Those waste objects have huge potential, are survived for someone to design a second life for them. The [Recollida de Mobles i Trastos](https://www.google.com/maps/d/u/0/embed?mid=1l2VAhplHwkWYhNi6WOcDeqnxPoE&msa=0&output=embed&ll=41.40729743401368%2C2.1637915739745495&z=15) is a system, where once a week from 8 pm to 10 pm; you can pull (or find) furniture and junk along the street in the indicated areas of Barcelona. Each zone has its specific day. So, what did we do? We collected and repurposed scrap materials from the streets to use for our projects to customize our working space in IAAC. In our design proposal, the life cycle had to be well thought-through  in terms of reusability, portability, evolution.


One of the main goals was to learn how to mix the old with the new in relation to tooling (laser cutter, CNC, 3D printing and scanning), techniques and material resources, while working on the edge of the real and digital world. Following the idea of digital\local manufacturing. The boundary between what is on the one hand the real world (analogue, carbon-related, offline) and on the other hand the digital world (digital, silicone, online) has become increasingly arbitrary. The infosphere is not limited to purely online environments. In the real world, we are an inforg (an informationally embodied organism, entity made up of information); where we exist in the infosphere. How can we shape our infosphere?

We are natural agents in the physical world.  The human nature desire empathy, curiosity, beauty and that is what distinguishes us from robots, computers and AI. It is all about connecting new technology with creative thinking. The creativity is our capital which concerns those who are able to collaborate both with a machine and with a team of people. What I discovered while working on this project is the fact that cooperation with both people and machines has become equally difficult. This awareness helps us change our environment, perspective and point of view. It allows us to influence our own behaviour and the behaviour of other people. Where are the limits of human/technology?

***
Links:
[https://www.ted.com/talks/daan_roosegaarde_a_smog_vacuum_cleaner_and_other_magical_city_designs](https://www.ted.com/talks/daan_roosegaarde_a_smog_vacuum_cleaner_and_other_magical_city_designs)

[http://thomasdambo.com/](http://thomasdambo.com/)

[https://www.ikeahackers.net/](https://www.ikeahackers.net/)

[https://www.curroclaret.com/es/bio.html](https://www.curroclaret.com/es/bio.html)

[http://distributeddesign.eu/](http://distributeddesign.eu/)

[http://bomdesign.nl/](http://bomdesign.nl/)

[http://www.remyveenhuizen.nl/](http://www.remyveenhuizen.nl/)

[https://foresso.co.uk/](https://foresso.co.uk/)

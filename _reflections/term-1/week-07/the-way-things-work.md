---
title: 07. The Way Things Work
period: 12-18 November 2018
date: 2018-11-18 12:00:00
term: 1
published: true
---

# Every object tells a story

**Disnovation as an answer on “innovation propaganda”. This neologism is called perfect solution for economic recovery. The edge in the dialogue between dystopia and innovation offered by their programming questions the reckless headlong pursuit of progress.**

Designing for/with/within the first-person perspective for system.
Disnovation of [The Turtle](http://www.setupshop.eu) 100% recycled African car made in Ghana. DIY car assembled by local craftsmen. It is an extraordinary vehicle that is something like a pick-up and a jeep merged together. The car is designed by locals people and adapted to local needs.

[Melle Smets](http://mellesmets.nl/) researcher and artist together with sociologist Joost van Onna they decided to start a research about automotive waste from Europe, by tracing them they came to a Suame Magazine. It is an industrialized area in Africa where the waste from Western car is dealt with and sold by specialised technicians in informal workshops. The certain spare parts serve totally unexpected purposes and that a large recycling economy.  It could not work without a technical coordinator to overlook the manufacturing process and the linking with craftsmen and mechanics who will realise the project.

[![Trailer Turtle]({{site.baseurl}}/screen video.jpg)](https://youtu.be/tpigWsmYY3A)

The project prompts us to reflect on the transformation of industrial logic. Due to the idea of documenting a DIY manufacturing process, they created a driver for building a car from 0 to the last screw and follow the process from workshop to workshop to understand the process and then tell the story about the vehicle local “innovation”. The Turtle car is far from complying with the Western standards of circulation. For whom is the circular economy model for? Who do these rules apply to and who they set them? It shows us how circular is still not circular. In a bottom-up design methodology in our case are much more challenges than top-down and obstacles sometimes unstoppable which inhibit further activities.

But what is more valuable is driven by implication for society, social impact involvement. The product is developed by society to fit the context and adaptation of the project to their needs. This is maybe the beginning for the economy to change the value flow creation. Designer's role and design process in relation to valorizing research. In this case, a designer is not limited by a final product, the product is developed by society to fit the context and is coordinated by an engineer who became a part of the society to create the design system. Designer's responsibility for the impact of the project on society as a part of the solution.

---
### ..., INPUT, OUTPUT, INPUT,...

Our group installation based on the internet of things (IoT) where different inputs (publisher) trigger different outputs (subscriber). All thanks to our broker ( computer connected to the network) who was composed of two Raspberry PI boards it received messages from publishers and forward them to subscribers.

My group was working on the output in our as a part of our interactive installation. The Big Bang is a pneumatic/ shape changing/ colour changing project which is holding the viewer in suspense until an explosion of colourful confetti.  One of our starting points was [Superpower Studio](https://www.superpowerstudio.com/powers) projects where explores the twilight zone between useful and useless objects and takes the ‘unuseless’ to the next level and bring you something unnecessary awesome. In The way things work week my group had two goals: first was to improve our skills in programming the second was to have fun (or in reverse order). I think we have managed to achieve this. Starting from the led connection and how to control it to wireless internet connection between our projects on the ending.



---

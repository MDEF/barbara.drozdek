---
title: 06. Designing with Extended Intelligence
period: 5-11 November 2018
date: 2018-11-11 12:00:00
term: 1
published: true
---

# Coexistence for humans and AI


**The fact is that Artificial Intelligence is far from reaching human intelligence. But it is changing, AI learns, analyses, adapts. We are helping AI through training it. The development of AI is dependent on data (which is always biased),  it gets an information from the environment. Without data, an AI wouldn’t be able to make predictions.**

<image>

We are now in the era of implementation where what really matters or I think it matters is execution, system/product quality, speed and DATA.

Does AI completely replaces the individual jobs? On the graph, I am trying to show the area where AI and human will come in our professional environment. How Artificial Intelligence has an impact on potential job replacements in relation with humans?

But what's more serious than the loss of jobs is the loss of meaning, because the work ethic in the industrial age, has brainwashed us into thinking that work is the reason we exist, that work defined the meaning of our lives.

How might AI impact mankind, work and coexist with mankind?  I think that really, AI will take away a lot of routine jobs, but routine jobs are not what we're about. We should create jobs of compassion, we will need a lot of social workers to help us make this transition.

How do we differentiate ourselves, as humans in the age of AI? What does make us human, if this artificial intelligence can learn, create, and communicate?  The answer is memory. A 5W method, where the memory is as an azimuth/reference point for us. We use it to answer the questions: Who? What? Where? Why? When? We as a human animal are driven by the instinct that machines do not have it. And we can always differentiate ourselves with the uniquely capable jobs that are both compassionate and creative, using and leveraging our irreplaceable brains and hearts.

When AI will take away the routine jobs, it will become a great tool for the creatives, so that scientists, artists, musicians, designers and writers can be even more creative. AI will work with humans as analytical tools that humans can wrap their warmth around for the high-compassion jobs.

Could AI be a tool in design to great user experience? What if the AI will shape the user experiences of tomorrow?

In actuality by implementing AI into products integrated with AI that interact with people. Our goals as designers are research and development the interaction between the user and the system. This interaction can be supported by AI to analyze, learn and adapt to the person who is using it. Using this opportunity we as the designers should understand and shift our way of thinking about systems and products that they can analyze, learn and adapt during the process of using. The interaction between the system or product with the user, it is the process of development itself.

The development of AI-technology relies on trust.  How the decision has been made? Who is a rational agent? Who is controlling who?  In the aspects of ethics like safety, fairness, justice, discrimination.

How huge responsibility we have as the creative community?

 *“technology is neither bad or good, it is really a reflection of the human that is creating it”.*
Chris Duffey, Head of AI Strategy & Innovation at Adobe

It is completely not inclusive. We need to be aware of how those decisions will have an impact on society. Because society is using this bias information the most. We are not completely separate from nature/society/opinion. The machine is based on biased data it is not a racist/discriminator it depends on a data it uses. It is about the input. We can not fix it but we have to be aware of it. We as a creative community are connecting the machine with the social system.

The machine is not contexted aware. A product is only as good as your data is. To be aware of our intention, You want to build for/with people, The input(data) is always human generated.

---
References:

![](https://99percentinvisible.org/episode/children-of-the-magenta-automation-paradox-pt-1/)

![](https://www.technologyreview.com/s/513696/deep-learning/)

![](http://www.kylemcdonald.net/)

{% figure caption: "*Markdown* caption" %}
![]({{site.baseurl}}/transfolab.jpeg)
{% endfigure %}

---
title: 09. Engaging Narratives
period: 26 November - 2 December 2018
date: 2018-12-02 12:00:00
term: 1
published: true
---

# The power of story

**Write what you know. Put something about your own life how do you feel. Do you feel scared? Do you feel alone? So why write about what you know? This is the power of a story that has the ability to connect with people on an emotional level.**

[![]({{site.baseurl}}/pixar.png)](https://www.youtube.com/watch?v=D4NPQ8mfKU0)
*click on the photo to watch*

Well, it's probably what happened to you made you feel some particular way. When you try to tell a story its to get the audience the same feeling. It’s not about telling the story once or twice but only retelling the story many times does it really sparkle. The key in storytelling beyond the structure and authenticity is a creation of awareness. Creating awareness in stories is mainly about triggering emotional reactions. Joy 😀, surprise 😯, sadness 😔, anger 😡, fear 😱, disgust 😖. It is strongly connected with our personal experience as I mentioned before. Unless by creating the scariest rollercoaster experience, probably want to relieve your user for a negative emotion and aim for joy in most cases. We are all storytellers. It's something we do naturally and start doing as children. The first story that I watched and I've already known what is the storytelling  was Pixar’s  animation *Luxo Jr.*

A Nigerian storyteller Chimamanda Ngozi Adichie's she is also a novelist. In her talk, she points out the fact that we as children are impressionable and vulnerable in a face of a story. Do we identify with a story from books, movies or just verbal stories? What does make you that you feel part of the story?  On the other hand, how the single story, shows people as one thing by repeating and duplicating the same stories and that is what they became. how much power does one story bring with it? What is a power in this case? Power is a capacity to tell a story about another person and make a definitive story of that person. The single story creates a stereotype and the problem with it is that shows only one side of the reality it is uncompleted. How many reality has a good story? The story has been used to dispossess and to malign but it can be also be used to empower and to humanize. She says that story can break the dignity of a people but the story can also repair and that broken dignity. I really like how she ended her talk:

*When we reject the single story, when we realize that there is never a single story about any place, we regain a kind of paradise.*


[![]({{site.baseurl}}/nigeria.png)](https://www.ted.com/talks/chimamanda_adichie_the_danger_of_a_single_story?referrer=playlist-how_to_tell_a_story)
*click on the photo to watch*

Last week I was thinking why a guy from MIT Bjarke Calvin the author of  [duckling](https://www.duckling.co/) project that empower people socially with storytelling  wants to persuade or encourages us to start using one more new social platform. It is interesting to observe and try to analyze what kind of story the social media creates nowadays. What kind of stories do we create in social media on our profiles? Whether are we entitled to determine their stories? What kind of narrative does it bring? I think it doesn't bring any insight. From my perspective not so much active person on my profile but much more active like a follower or observer of other’s profiles. I am overwhelmed by an excess of information that does not have much value to me. I have the ability to quickly assess whether I like or I do not like if it had some impact in my life. Who cares? It is harder to fall asleep after long time of scrolling the photos on Instagram at bedtime, I know from the autopsy. But at least you know how Kim contours her face today.

Bjarke’s project is for me a combination of words and images passes the message I want to share. A real story told from my perspective. But how many realities has a story? Reality can be subjective, objective or intersubjective. The most important for me is to learn something new from those stories and keep up curiosity. The measure of success is that good stories get sheared. I will try to move from social media to insight media and be more active there than on my Facebook and Instagram profiles. This is my first try. You can check it here.


 [![]({{site.baseurl}}/duck.png)](https://duckling.me/b_drozdek/5784653754517)
 *click on the photo to check*

---

My project draft version, see more in the last week of the 1st term.


 [![]({{site.baseurl}}/planet.png)](https://youtu.be/Ozv6LvYGDkY)
 *click on the photo to watch*



My area of interests I feel that I am dancing around one topic: new material. The new/biomaterial. The question is what is new? Has not now "new" ever had a place in the past? However, it was not widely recognized by all of us. Here I am reminded of my childhood in a small village in central Poland. And all the weird things that my grandmother usually did like crocheting rugs from recycled textile straps or making glue for wallpaper from starch and water. The need is the mother of the invention. In those cases I mentioned earlier, the context is similar in the same living space. What types of processes have we used to do in our living space, and what are we doing now? What links to new/biomaterials? The fact is that everything that we connect, regardless of our interests and lifestyle, is that we all produce biowaste.

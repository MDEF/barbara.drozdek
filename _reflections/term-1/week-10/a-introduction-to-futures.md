---
title: 10. An Introduction to Futures
period: 3-9 December 2018
date: 2018-12-09 12:00:00
term: 1
published: true
---

# What will be next?


**What is the future? What is nature? Who decide that? How can we link the future scenario with reality? What influence does human have on the future and what is beyond our control?**

![]({{site.baseurl}}/3planety.jpg)
*Three spheres of the planet earth*

The futuristic utopia and dystopia in this case that i want to present is not as usually present by the media techno-centered but is nature/techno-centered. This is the future that doesn't exist. The speculation full of  probabilities and possibilities shaped the vision of the future in more critical futures-oriented mindset.  What we can do? We can still treat the nature.  But how do we move on from here? How do we want to solve the important issue like global warming, climate change, decrease of biodiversity and urbanization? We have to understand our relationship with nature. How?

To understand our relation and predict how it is going to shape in the future we have to start from the beginning. It is about time and our possibilities on the planet earth that first was a rock with the geosphere floating in space, the next stage was panet with biosphere and then we emerged. What is going to be next sphere? Is it a technosphere? The sphere of all human technology. We are living in the world that has been designed already. What is the most natural thing around us? Is it you?

**BORN AND MADE  ARE FUSINING**

**NATURE AND CULTURE ARE FUSINING**

Is the nature and technology are opposite? Or they are shitfining in natural way that mean that the technology is naturalised by us. The example of mobile phone witch  now is part of our identity and our lifestyle. I am old enough to remember the life without it. Now when I leave home before the party always count to three (wallet, keys, mobile phone) if you don't have it with you feel that something is missing anan none of this things are not a part of our body. It is part of our nature. We are catalysts of change because nature is changing along with us. Nature is not an esthetic entiti from the past that has to be saved. Nature is dynamic and from our perspective. We are coevolving with technology, we have created an emerging technosphere with is very complex and it is bigger that us, have found the way you guide the way it grows and don't think about an illusion of total control. We will need the ecological intelligence to balance those two spheres biosphere and technosphere. Because all of the shifting processes of those two spheres shaped our planet.

**How technology becomes nature.**
We are coping with technological change. How is it going to look like in 10, 50 100 years? From the stone axe to the mentioned before mobile phone. The piramithe of technology by Koert Van Mensvoort shows how the technology becomes nature in seven steps. Inspired by Abraham Maslow’s pyramid of human needs.  People have different needs from food to self realisation. According to Maslow every lower needs has to be fulfilled before you can move on to higher level.

![]({{site.baseurl}}/maslow.jpg)*Maslow's pyramid*

![]({{site.baseurl}}/technology_pyramith.jpg)
*The same model translated by Koert Van Mensvoort on technology*

1. The lowest and the first level is the envisioned stage. Every new idea before it comes accepted by our society. It is a dream, an idea or vision that how all of technology starts. As like time machine which stopped on not working prototype. It is a level of dreamers, poets and science fiction writers and it it underestimate by more practically oriented people. It is a beginnings of new technology. As like communication satellite witch  lifted on the second level the operational level.
2. Operational stage it is a working prototype exists. Al like growing meat by cultivating the cells of the animal without sloth the whole animal.  
3. Applied level technology becomes available and affordable. On this stage there are some technology that are stolen for example the solar panels. It’s existed from decades but from the very long time they just not cost effectively enough to be widely applied. The same the nuclear technology witch is been applied in many countries but is never truly accepted because of the objections against nuclear waste and risk of nuclear disasters. It shows that technology doesn't rise and expand without context of other technologies competing of our acceptance.
4. The accepted. Some of technologies rich the acceptance level in only few years like a smartphone, a tv, a car or GPS systems.
5. The vital level. It is a level where is hard to live without. We can take the example from the level before the GPS system. How many of us can read the map? How many of us can move from one point of a city to another without GPS?  If you remove the technology from this level from the society it would start lifestyle changing crisis.  Another example is sewage system or internet.
6. An invisible technology. An alphabet that the information technology that allow us to make our thoughts voices permanent in the physical state. Or a clock the next example of invisible technology it allows us to measure time with more precision that the natural. We learn to be a master in language then we forget about tin we don't think about it when we are using it.
7. And on the top of the pyramid naturalised level. Clothing for us it is some natural that we do not think about negating this. An agriculture now it is something natural but 10 000 years ago it was a big technological revolution. We changed our lifestyle from hunting and gathering things on the savanna we settled down and started planting. It change our lifestyle but it was also a radical intervention in natural environment at that time. Cooking technology was a revolutionary new invention and for the first time  we start preparing your food before we eat it. By predangesting we were able to intech more calories in less time.

Analysing this piramid we can notices that the technology always starts as something artificial that becomes something familiar when it is successful it can be a second nature the highest level when it is a part of our life and we can't imagine life without it, it is the first nature.


**A debate discusion.** A desire to design our an environment according to the idealised image of unspoiled nature. How technology becomes invisible part of our environment up to the level that we don't recognize it as technology any more.  We would like to walk in untouched landscape with our cell phone. The simple example of this paradox is the antenna tree. Is it a tree? Is it an antenna? Is it post-optimal? It is not a tree.  Is it a critical design or human simulation of the nature in environment to prevent the horizon-pollution of the GSM-antenna even that more than 30 meters high “tree”.  But is it really about aesthetic values of our image of the unspoiled landscape? Or with the concealment of these antennas critical questions on the possibly detrimental radiation are being avoided.

![]({{site.baseurl}}/tree.jpg)*The dutch company [Kaal Masten B.V.](http://www.kaal.nl/) antenna tree.*

This metal tree is not nature, it is simulation it is a picture of nature like landscape painting of Monet. What we think about the nature is a simulation or it is what we expected. What we can do? We can still treat the nature.

Ex.1 Unthinking future.
Don't forget about timeframe. It helps us to do not overthink the idea/image of the future.

![]({{site.baseurl}}/cards2.jpg)

![]({{site.baseurl}}/card1.jpg)

![]({{site.baseurl}}/card15.jpg)

---
title: 02. computer-aided design
period: 31-06 January/February 2019
date: 2019-01-28
term: II
published: true
---
## --> 2D & 3D modelling

 I decided to model and parametrize the open source design of the italic shelf by Ronen Kadushin. My design decision was due to the fact that today we have many open source projects. The Internet is full of free downloads ready for fabrication. Unfortunately, it does not give us the possibility to personalize the objects that we would like to do.


 For modelling and parameterization I used a program for prametric design grasshopper that is an extra function in Rhinoceros modelling tool. The software is for free but to run it you need a license for Rhinoceros (90 days trial version).

![]({{site.baseurl}}/code.png)
*the script design*

The whole design process is explained step by step in the script project. Divided into stages in accordance with the logic of design in a program such as a grasshopper. At the beginning, it defines parameters that are variables in the project. Then every logical step is grouped into one cloud. We start the code with the parameters, then proceeding to 2D modelling and then to its 3D deployment. Each change in the script can be observed in the preview in the window Rhinoceros.

![]({{site.baseurl}}/model_code.png)
*code and preview of design in Rhinoceros*

To export the file from grasshopper to Rhino and make in "physical in digital" you have to choose the geometry that is the final result in the code.
By using right click select bake option (icon with the fried egg :>). Then select the layer in Rhino where you want to save the object.

![]({{site.baseurl}}/bake.png)
*baking the object into one of the layer in Rhinoceros*

What you can change in design (by using sliders):
* shelf width
* depth
* number of shelf levels
* the height of each shelf level
* the thickness of the board from which the shelf is made
* distance between crossbars

**You can play with it like this!**
 [![]({{site.baseurl}}/video.png)](https://youtu.be/vQtijsuTvyc)
 *personalization*

## --> rendering

I used Fusion360 to do a quick render of the italic shelf.

-- **upload** the .obj format file into Fusion360 and open it with double click on a model

![]({{site.baseurl}}/upload.png)
*uploading the object to Fusion360*

-- change the **model** mode to the **render** mode of the files
-- select all **objects** in the **bodies**

![]({{site.baseurl}}/select.png)
*managing with objects in Fusion360*

-- in the **Setup** select an option **physical materials**
-- go to physical materials in **library**, choose material **wood** -in my case I selected pine. To apply material on the object is enough to drug and drop it on the shelf.

![]({{site.baseurl}}/material.png)
*picking the material*

-- in the **Setup** select an option **scene settings**
-- go to scene settings in **settings** change colour of the **background** on white

![]({{site.baseurl}}/color.png)
*picking the background*

-- in **environment library** go to **library** pick the light. I picked soft light and did the same what l Have done with material drug and drop light on the object.
-- **render**, pick local render, then selected web version and the quality of the result

![]({{site.baseurl}}/rendering_settings.png)
*rendering settings*

-- when the rendering process is finished go to **rendering gallery** and **download** the file

![]({{site.baseurl}}/render.png)
*pine wood final render*

### --> To download:

[files](https://gitlab.com/MDEF/barbara.drozdek/blob/master/files/week_2_aided_design_files.rar)

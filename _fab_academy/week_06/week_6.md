---
title: 06. electronics design
period: 28-06 February/March 2019
date: 2019-01-28
term: II
published: true
---

I redraw the echo hello-world board in two softwares Eagle and Kicad but I found Eagle more intuitive. To the drawing l added button and one more LED and resistor.

## --> drawing

Based on the hello-world board drawing I added a button and two LEDs. To do a drawing in Eagle (electronic design software) I followed a Fab Academy class with one of our classmate. What was important and what we did as a first thing we added Fab Library that I downloaded from the Fab Library/ on GitHub. The next step was to create a new file to be able to create a new schematic and start building your design based on the components from Fab library that includes all of components that are ready to use in our workspace. When the design is finished I mean when all components are connected on your schematic you go to board file to places all of component on a board preview. Is it important to find optimal position of components to do not allow traces to cross. But if it happen anyway you can add 0 Ohm resistor to the schematic. It works as a bridge in your design.
After the first classes that we that I needed to add some more components to the drawing anyway. But at least I had already known how to use the software.

![]({{site.baseurl}}/schematic.png)
*schematic in Eagle*

![]({{site.baseurl}}/mistake.jpg)
*board design*

On the end I checked the board by using DRC. I go to Tools, select DRC (design rules), load and check for errors. It shows me two errors in connection because I added to 0 Ohm resistors without connecting them on my schematic. What I did I went to the schematic and connected them in a place that I needed. To export the board I selected only the top layer and the dimension layer to export it as a monochrome image with 1000dpi.

**List of components:**

1x 6-pin programming header

1x Attiny44A Microcontroller

1x FTDI-SMD-Header

1x Resonator - 20MHz

1x Resistors - value 10k ohms

1x Resistor - value 499 ohms

1x Capacitor - 1uF

1x Button - IMPORTANT! It has a direction so be aware that you place it along the lines that you can notice one a bottom when you flip it.

2x LED - IMPORTANT! The side with a green line is the cathode so the right way to solder it is to place it to the GND

Issue 1.

I had a first mistake in design the board here. I realized that after the fabrication. So I had to unsolder all of the components that I soldered. I was missing one VCC connection from 10 kOhm resistor. I realised that when I was trying to program it and the board was not powered from Fab ISP.

![]({{site.baseurl}}/good.jpg)
*board design after corrections*

![]({{site.baseurl}}/week6vfinal.png)
*designed board.png file*

---

## --> cutting

This week assignment I have done using a vinyl cutter to make flexible echo-hello board. For this I prepared the composite made out of plastic(PVC) sheet and copper tape. To cut the file on the vinyl cutter you need a .png format of your file.

![]({{site.baseurl}}/composite_preparing.jpg)
*composite preparing*

![]({{site.baseurl}}/vinyl_cutter.jpg)
*vinyl cutting*

I repeated cutting process because the blade caught on copper tape which caused damage to the board and also during the cleaning no needed parts from the drawing I did some mistakes too.

![]({{site.baseurl}}/tweezer.jpg)
*removing no needed elements*

---

## --> soldering

I collected all of components and updated the Fab Academy google sheet with components.

![]({{site.baseurl}}/components.jpg)
*list of components*

For soldering I used the lowest temperature of the soldering gun (300) because the synthetic material of my board was melting very fast so I had to be aware to not keep it too long touching it. Because it was my second time soldering this material because of the first mistake of design I had some practice already.

![]({{site.baseurl}}/soldering_elec.jpg)
*soldering*

![]({{site.baseurl}}/final_shot.jpg)
*final shot*

---

## --> to download

[files](https://gitlab.com/MDEF/barbara.drozdek/blob/master/files/week_6_electronicdesign.rar)

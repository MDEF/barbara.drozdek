---
title: 14/16. Machine Design
period: 30 April - 08 May 2019 & 15-22 May 2019
date: 2019-01-28
term: II
published: true
---


This submission was done during our classed in the first term. During the From Bits to Atoms week leaded by Fab Academy team. Here I want to just briefly present it.

DJ POM POM is a Christmas machine to make a pom pom. It has a rotating platform in which yarn can be wrapped. You can customize it by choosing the colour of the yarn and the size of the pom pom by defending the position of the sticks in a star platform. The machine operates accordingly to the user interest in Christmas.

![]({{site.baseurl}}/dj_pompon.png)
*dj pom pom machine*

Full project documentation you can find [here](https://mdef.gitlab.io/dj-pom-pom/).

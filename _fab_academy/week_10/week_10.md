---
title: 10. input devices
period: 28-03 March/April 2019
date: 2019-01-28
term: II
published: true
---

In this week I worked With Gabi on the input device. We started to work together because we wanted too build the pottery wheel together during the mechanical and machine design. That is why we decided to choose the HAL sensor as an input to measure magnetic field of the spinning disc.

![]({{site.baseurl}}/10_hello.mag.45.png)
*hello hal board*

We picked hello.mag.45 board from all input devices that Neil presented on our classes. We found all of detail information on the Fab Academy [website](http://academy.cba.mit.edu/classes/input_devices/index.html). In our case we did not have to design the board so we used this one that was designed by Neil. We downloaded the .png file from the prepared it for milling using the [Fabmodules](http://fabmodules.org/) website.

##-->milling

First the incut:

![]({{site.baseurl}}/10_incut_lines.png)
*incut traces*

![]({{site.baseurl}}/10_settings_incut.png)
*incut settings*

Than the outcut:

![]({{site.baseurl}}/10_outcut_lines.png)
*outcut traces*

![]({{site.baseurl}}/10_settings_outcut.JPG)
*outcut settings*

![]({{site.baseurl}}/10_milling.JPG)
*milling*

##-->soldering

**List of components:**

1x  IC A1324

1x IC Attiny45

1x Resistor 10k

1x Capacitor 1uF

2x 3 pins connector

1x 6 pins connector

On previously milled board we started to solder component from the list above. It was not difficult because we both have some experience from electronic design week submission.

![]({{site.baseurl}}/10_tobesolder.JPG)
*in the process of soldering*

![]({{site.baseurl}}/10_soldered.JPG)
*soldered hello.mag.45 board*

As always after soldering we have to check the continuity between the components in the board. We use multimeter to do this in the mode with rotated (90 degrees) an icon of WiFi.

![]({{site.baseurl}}/10_multimeter.JPG)
*checking the continuity*

For programming our mag board, Gabi connected used the FabISP that she has done previously. She connected them using 6 pins connector cable in the way both connect in the right order. She connected VCC to VVC and GND to GND on FabISP.

##-->programming

For programming, we used Fab Academy student documentation Nicolo Gnecchi.
The link to his website is [here](http://fab.academany.org/2018/labs/barcelona/students/nicolo-gnecchi/input-devices/ ).

![]({{site.baseurl}}/10_programing.JPG)
*connections*

In this case she used the terminal to program the board with the hal sensor.

Steps:

-- Open terminal and type:

--> make -f hello.mag.45.make

After this commend it compiled the code and generated two new archives:

* .hex

* .out

-- Type:

--> make -f hello.mag.45.make program-usbtiny

On this stage we got an error with usbtiny connection, we were not able to program our board.

**Problem:**

*Gabi@DESKTOP-I5C1KB9 MINGW64 ~/Documents/MDEF/02_term/Fab Academy/week-11/files

$ make -f hello.mag.45.make

avr-objcopy -O ihex hello.mag.45.out hello.mag.45.c.hex;\

        avr-size --mcu=attiny45 --format=avr hello.mag.45.out

AVR Memory Usage

----------------

Device: attiny45

Program:     510 bytes (12.5% Full)

(.text + .data + .bootloader)

Data:          0 bytes (0.0% Full)

(.data + .bss + .noinit)



Gabi@DESKTOP-I5C1KB9 MINGW64 ~/Documents/MDEF/02_term/Fab Academy/week-11/files

$ make -f hello.mag.45.make program-usbtiny

avr-objcopy -O ihex hello.mag.45.out hello.mag.45.c.hex;\

        avr-size --mcu=attiny45 --format=avr hello.mag.45.out

AVR Memory Usage

----------------

Device: attiny45

Program:     510 bytes (12.5% Full)

(.text + .data + .bootloader)

Data:          0 bytes (0.0% Full)

(.data + .bss + .noinit)


avrdude -p t45 -P usb -c usbtiny -U flash:w:hello.mag.45.c.hex

avrdude: initialization failed, rc=-1

         Double check connections and try again, or use -F to override

         this check.



avrdude done.  Thank you.

make:  [program-usbtiny] Error 1

Gabi@DESKTOP-I5C1KB9 MINGW64 ~/Documents/MDEF/02_term/Fab Academy/week-11/files

$*

We tried again to program it with the AVR mk2 from the Fab Lab, but without success. The next try to fix it was to unsolder and solder again few components from the board that seam to be not well attached to the board. After this we repeated the process of programming. The board was finally programmed.

**Program:**

*Gabi@DESKTOP-I5C1KB9 MINGW64 ~/Documents/MDEF/02_term/Fab Academy/week-11/files

$ make -f hello.mag.45.make

avr-objcopy -O ihex hello.mag.45.out hello.mag.45.c.hex;\

        avr-size --mcu=attiny45 --format=avr hello.mag.45.out

AVR Memory Usage

----------------

Device: attiny45

Program:     510 bytes (12.5% Full)

(.text + .data + .bootloader)

Data:          0 bytes (0.0% Full)

(.data + .bss + .noinit)


Gabi@DESKTOP-I5C1KB9 MINGW64 ~/Documents/MDEF/02_term/Fab Academy/week-11/files

$ make -f hello.mag.45.make program-usbtiny

avr-objcopy -O ihex hello.mag.45.out hello.mag.45.c.hex;\

        avr-size --mcu=attiny45 --format=avr hello.mag.45.out

AVR Memory Usage

----------------

Device: attiny45

Program:     510 bytes (12.5% Full)
(.text + .data + .bootloader)


Data:          0 bytes (0.0% Full)

(.data + .bss + .noinit)


avrdude -p t45 -P usb -c usbtiny -U flash:w:hello.mag.45.c.hex

avrdude: AVR device initialized and ready to accept instructions

Reading | ################################################## | 100% 0.00s

avrdude: Device signature = 0x1e9206 (probably t45)

avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed

         To disable this feature, specify the -D option.

avrdude: erasing chip

avrdude: reading input file "hello.mag.45.c.hex"

avrdude: input file hello.mag.45.c.hex auto detected as Intel Hex

avrdude: writing flash (510 bytes):

Writing | ################################################## | 100% 0.47s

avrdude: 510 bytes of flash written

avrdude: verifying flash memory against hello.mag.45.c.hex:

avrdude: load data flash data from input file hello.mag.45.c.hex:

avrdude: input file hello.mag.45.c.hex auto detected as Intel Hex

avrdude: input file hello.mag.45.c.hex contains 510 bytes

avrdude: reading on-chip flash data:

Reading | ################################################## | 100% 0.59s

avrdude: verifying ...

avrdude: 510 bytes of flash verified

avrdude done.  Thank you.*

##-->testing

We were searching some examples in the FabAcademy search engine to find some examples. We decided to use the files created by Carolina Vignoli. You can find it [here](http://archive.fabacademy.org/fabacademy2016/fablabbcn2016/students/346/Week16.htm).

She used the Processing software so we opened hers file in Processing, we changed the port number according to the one of the computer (COM4) and tested if it read the sensor or not. We got a magnet from Fab Lab room to play with it. It works!!!!

![]({{site.baseurl}}/10_stage1.JPG)

![]({{site.baseurl}}/10_stage2.JPG)

![]({{site.baseurl}}/10_stage3.JPG)
*testing*

## -->to download

[files](https://gitlab.com/MDEF/barbara.drozdek/blob/master/files/week_10_input.rar)

---
title: 01. project management
period: 23-30 January 2019
date: 2019-01-28
term: II
published: true
---

23-30/01/2019

This is the first week of Fab Academy for me. Last week we (group of MDEF students) were in China on a research trip. So this is a time to start the adventure. I have my website already. I made it last term, so I will describe what kind of programs/tools I use and shortly describe how I added a tab of Fab Academy on my website.

I downloaded and installed GitBush because I use Windows software. The instruction how to set up GIT step by step you can find [here](<https://www.youtube.com/watch?v=54mxyLo3Mqk>) and the [video](<https://www.youtube.com/watch?v=54mxyLo3Mqk>).

I created an account on GitLab website and I created a new project. My is :[https://gitlab.com/MDEF/barbara.drozdek](https://gitlab.com/MDEF/barbara.drozdek)

I navigated to the place/local folder on my computer where I wanted to clone my personal repository from the GitLab website. In the local folder there is located all of the content from my website.

First I was using [MkDocs](<https://www.mkdocs.org/>) and the Fab Academy template from my website.

For web development I use a text editor [Atom](<https://atom.io/>). After a few months of using it I am still not familiar with it so maybe I will change it on [Brackets](<http://brackets.io/>). I pushed all of my changes in my repository folder through the Atom.

![]({{site.baseurl}}/gitflow.jpg)

I changed MkDocs to [Jekyll](<https://jekyllrb.com/>) to transform my plain text into static website and make it more stylishly based on layouts. You can download it [<https://jekyllrb.com/>](<https://jekyllrb.com/>) and find some tutorials how to installed it [https://jekyllrb.com/docs/installation/#guides](<https://jekyllrb.com/docs/installation/#guides>).[ https://jekyllrb.com/docs/](https://jekyllrb.com/docs/). But first install Ruby Development Environment and them install bundler and then finally jekyll. Some guides to create a [jekyll site](<https://jekyllrb.com/docs/>)

I copied and replaced the files from my local repository and copied the content to my new jekyll template. I pushed all of the changes using Atom.

## --> Last week

I added a new "FabAcademy" tab to my website.

* I created a new folder in Atom "_fab academy_" and added the folder "week_1" in this folder I placed the md file.

![]({{site.baseurl}}/Inkedfabacademy folder_LI.jpg)

* In the "_includes_" folder I went to the header.html file and I added that line that defines the "FabAcademy" tab on my website.

![]({{site.baseurl}}/Inkedheader_LI.jpg)

* In the "_layouts_" folder i added the "fab_academy.html" file with the structure of the "FabAcademy" tab inside.

![]({{site.baseurl}}/Inkedlayout_LI.jpg)

* I added the "fab_academy.md" file in my project folder where inside I defined the layout of the "FabAcademy" tab, the title of it and permalink that shows up in the browser.

![]({{site.baseurl}}/Inkedfabacademy_md_LI.jpg)

* Last but not least the "_config.yml_" I added to the custom collections
collections  and defaults --> "fab_academy" lines

![]({{site.baseurl}}/Inkedconfig_LI.jpg)


 Always when you change something in "_config.yml_" go to the terminal and in your repository folder confirm changes by building the site and make it available on a local server!!!

![]({{site.baseurl}}/terminal.png)

How to upload and push files?

* All changes in your .md file you can save using the shortcut **Ctrl+S**
* Then on a right top corner stage all of **Stage All** changes
* Add a commit message and commit changes by **Commit to master**. Adding the description of the change will help you remember the changes you made when you look at the branch of changes in the pipeline tab on Your GitLab project website)
* Push changes online or first pull then push if it is necessary

![]({{site.baseurl}}/pushing_steps.jpg)

---

References:

<https://hackmd.io/>

<https://internetingishard.com>


links:

<http://fab.academany.org/2019/labs/barcelona/local/wa/week1/#/start/gitlab-basics>

<http://academy.cba.mit.edu/classes/project_management/index.html>

support presentation: <http://fabacademy.org/2019/recitations/version-control/index.html#27>

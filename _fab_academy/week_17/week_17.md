---
title: 17. wildcard
period: 22-29 May 2019
date: 2019-01-28
term: II
published: true
---

--> 3D printing with clay

This week we could pick the technique that we want work with. We had proposed a lot of different options: wood banding, welding, 3d printing with clay using the industrial robotic arm. I picked the last option because it is connected with my final master project. Due to I want to 3D print the gestures of the craftsmen it was the perfect opportunity to test my geometries.


1. Preparing the file:

I created my geometry in Rhinoceros based on the information that I got from the Leap Motion sensor. It is not important right now I want to explain how to prepare the file for 3D printing and what are the rules to do this properly.

* First tip - the designed geometry should not has angles more than 45 degrees ( because of the gravity force it can simply collapses )

The geometry from the rhino you have to import to the Rhino parametric design program Grasshopper. Grasshopper has a special plug in calls [Taco](https://www.grasshopper3d.com/group/taco-abb-robot-programming-for-grasshopper). It allowed us to control the robotic arm and export the file that the machine can read. You can find the files on the bottom of the website.


* To prepare geometry first you have to divided it on lines. It is going to be a trajectory of the robot's movement.

![]({{site.baseurl}}/17_geometry.png)
*script geometry*

* All the settings of the rebot position have to be the same as it is saves in the machine. Here you can change the level of the bed, the start point and the end point of the file and even type of the movement that robot does.

![]({{site.baseurl}}/17_robotsettings.png)
*settings of the robot in the script*

* I always run the simulation of the robot that is a component that allow you to check the whole movement of the robot in the preview in the Rhino window.

![]({{site.baseurl}}/17_robotpreview.png)
*Rhino preview*

* To save the data - gcode - you have to copy it from the panel in grasshopper and save it as a text but in the format of the file that machine can read (.prg)

![]({{site.baseurl}}/17_datasave.png)
*exporting the gcode*


2. Preparing the material:

First prepare the clay to get the right thickness. We added a bit of water into the clay that we bought before in the ceramic shop [Angela Colls](https://www.google.com/search?ei=0Pb4XMHrCsmPlwT1wawo&q=ceramic%20shopping%20barcelona%20angel&oq=ceramic+shop+barcelona+ang&gs_l=psy-ab.1.0.33i22i29i30.5610.8917..10340...3.0..0.152.687.0j5......0....1..gws-wiz.......0i71j0i22i30.NRfzpR5_kQk&npsic=0&rflfq=1&rlha=0&rllag=41383661,2172781,612&tbm=lcl&rldimm=2190072460294551577&lqi=CiBjZXJhbWljIHNob3BwaW5nIGJhcmNlbG9uYSBhbmdlbFpECiBjZXJhbWljIHNob3BwaW5nIGJhcmNlbG9uYSBhbmdlbCIgY2VyYW1pYyBzaG9wcGluZyBiYXJjZWxvbmEgYW5nZWw&phdesc=RQHbjq39WGY&ved=2ahUKEwjKv4um3tTiAhVIqxoKHX7bCvsQvS4wAXoECAoQMw&rldoc=1&tbs=lrf:!2m1!1e2!2m1!1e3!2m1!1e16!3sIAE,lf:1,lf_ui:10#rlfi=hd:;si:2190072460294551577,l,CiBjZXJhbWljIHNob3BwaW5nIGJhcmNlbG9uYSBhbmdlbFpECiBjZXJhbWljIHNob3BwaW5nIGJhcmNlbG9uYSBhbmdlbCIgY2VyYW1pYyBzaG9wcGluZyBiYXJjZWxvbmEgYW5nZWw,y,RQHbjq39WGY;mv:!1m2!1d41.409632300000006!2d2.1935644!2m2!1d41.378845999999996!2d2.1547655!3m12!1m3!1d14934.182547745833!2d2.17416495!3d41.39423915!2m3!1f0!2f0!3f0!3m2!1i453!2i479!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!2m1!1e16!3sIAE,lf:1,lf_ui:10). It is super hard to say how much water you have to add to achieve the right thickness you have to feel it. My ex employer use to say that when the clay looks glossy its perfect to use.

![]({{site.baseurl}}/17_mixing.jpg)
*mixing the clay with water*

When the clay is smooth I started to fill the tube using the spatula. You have to be careful to not get the air in. Every air bubble inside during the process of printing can explode. I had my own technique of filling you can check it on the video below.

[![]({{site.baseurl}}/images/research_trip/17_mix.png)](https://www.youtube.com/watch?v=t0s-RQfMxF4)
*mixing the clay - video by Gabriela Martinez Pinheiro*

3. Preparing the machine:

After you turn on the machine you usually has to wait a bit to let machine powered on. When your cartridge is attached to the folder that is placed on the 6th axis of the arm you can start calibrating it. Four points calibration is necessary always to program machine where is the end f the tool which it is going to use. You have to touch with the tip of the tool the pointer in four different positions of the robotic arm.

![]({{site.baseurl}}/17_4points.jpg)
*4 points calibrating*

After this machine generates the position of your tool x,y,z coordinates. This numbers you have to paste into your code where you define the position of the tool. It should be the same on the machine settings and in the settings in your script.

![]({{site.baseurl}}/17_tool.png)
*tool position*

The last step is to open the generated .prg file that is on the USB stick. We first saved the file on the USB  and plugged into the control panel of the machine. Then you can pick you files in available files on the screen and run it. It is always a good practice to run the file in the air to check that all of settings as the high of the bed correctly or if the tool does not want to hit into table.

![]({{site.baseurl}}/17_running.jpg)
*the file running*

4. Printing:

For extrusion we used the air pressure. We controlled the power of the pump. The robot automatically opens and closes the valve, you can control it with one button on the control panel. It is important to adjust the speed of the machine to the pressure or all the way around to have nice continuous line of printed clay.

![]({{site.baseurl}}/17_printing.jpg)
*process of printing*

My first object collapsed because of the angle that was around 40 degrees. But I wanted to keep it because I like how the material acts in a dialogue with machine. The last line is like a signature of the machine in the object.

![]({{site.baseurl}}/17_finalshot.jpg)
*final shot*


--> To download

[files](https://gitlab.com/MDEF/barbara.drozdek/blob/master/files/week_17_woldcard.rar)

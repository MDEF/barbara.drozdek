---
title: Atlas of Weak Signals
period: 08 March - 12 April 2019
date: 2019-01-28
term: II
published: true
---

### Weak signal [definition]

**Weak signals** are early signs of currently small change. They can presage a strategic discontinuity. The search for weak signals is one where you do not know what you are looking for. It involves the identification of “not necessarily important things” which do not seem to have a strong impact in the present but which could be the trigger for major events in the future. Finding weak signals is one of the most challenging tasks in futures research and their analysis often leads to the identification of wild cards.

### 25 weak signals that we have worked on:

Surveillance Capitalism
- **Attention protection**
- Dismantling filter bubbles
- Circular data economy
- The truth wars (Fake news)
- Redesigning social media
- Manipulation

The Anthropocene
- Longtermism
- Fighting Anthropocene conflicts
- Carbon neutral lifestyles
- **Interspecies Collaboration**
- Climate Consciousness

Life after AI - the end of work
- Future Jobs
- **Human-machine creative collaborations**
- Fighting AI Bias
- Tech for equality
- Making UBI work

The End of the Nation-State
- Making world governance
- **Rural futures**
- Pick your own passport
- Refugee tech
- Welfare State 2.0

Kill the heteropatriarchy
- Non heteropatriarchal innovation
- Imagining Futures that are non western centric
- Reconfigure your body
- Gender Fluidity
- Disrupting ageism

### What this course gave me?

My first thought was not a linear way of thinking. I think that this is one of the most important skill of the (emergent) future designer. I see these as hidden skills that you can not measure. I realized that l was trying to do this last year when I worked on my thesis and the bachelor's project. But not very successfully. Linking and be aware what is going on around us. How systems and mechanisms work? Wow does it affect us? What influence do we have on it? How can we start improving it by starting from ourselves?

I found that all the techniques that Marian Quintero presented to us on our classes are very useful. How we can speculate about the future of the event / movement / information / decision / article. They help me with landing in different kind of scenario of the future. The flow of working every week in different group of people on potential weak signals topic was sometimes super fun sometimes frustrating but on the end the goal was to learn from each other.  We should consider what is the best way to present our work based on techniques suggested by coordinator.  In this case it was also a lesson how to  proper present a group work/project.

The last group work perfectly shows and in a way, it sums up what I should know after this course? What I still miss? My group was my group was a combination of people interested in materials and syntetic biology.  We started with semiotic map that I found very useful. Our starting point was Material/Bio. We were trying to connect it with some of the 25 potential weak signals.
By answering the questions:

### **Why is it important?**

### **What is the topic?**

### **How does it affect on ...? How is it connected to the rest of topics?**

In the same time we did data scraping to find the keywords from different topic that might overlap. Data scrapping was useless on the end because the key words that we got from the python code  were too general. Therefore, at the final presentation, we decided to create a semiotic map where the starting point was our topic as well but we did combined answering above 3 question, our ideas/visions connected with our personal projects and searching the topics that was repeated in our brain storming conversation.  We created visualisation of our topic with [kumu.io](http://kumu.io). It is online platform to map data that you can share and co-create with other users (I learn that creating this kind of map online is more useful than doing it on a paper that is good starting point, I think). At the end we finished with the project of the platform for configuring materials, new type of job (bio "Matter" influencer) and tagging materials for their future life.

![]({{site.baseurl}}/mapping_brain.png)
*visualization of the brainstorm | link [here](https://embed.kumu.io/d63698d2aac84803bdd4dba1cc6bfbaf)*

All of weak signals that we included to our semiotic map are interesting for me. I realized that the research that on first sight seams to be mainly about material is much more complex. It is connected with identity, Anthropocene, future of jobs (the most surprising), politics, surveillance capiltalism and more. Still my main area of interest is material as it was on the beginning of the master. But I dance around it in different field of design, perceive it from different perspective.

### **Weak signals relevant to my project:**
- Circular data economy
- Human-machine creative collaborations
- Future Jobs
- Climate Consciousness

![]({{site.baseurl}}/semiotic_map1.jpg)
*hand drawing draft of semiotic map*

I am preparing the online visualisation of this map connected with potential weak signals.  It is going to be available [here](https://kumu.io/bdrozdek/master-project#embodied-knowledge-in-digital)

---

I am still do not feel confident in using all methods and techniques that help to land your vision in a context. But I feel that my thoughts are getting more structured and I hope till the end of the III term of the MDEF masters you will see that I am improving myself in a specific context of my project.

**Tutors:**

Definition:

Mariana Quintero and José Luis de Vicente

Development:

Ramón Sangüesa and Lucas Lorenzo Peña

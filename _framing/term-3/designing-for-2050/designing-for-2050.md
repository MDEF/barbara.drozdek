---
title: designing for 2050
period: 25-03 April/May 2019
date: 2019-01-28
term: II
published: true
---
### **Collective imagination: Stories for the next billion seconds**

![]({{site.baseurl}}/internet.gif)

*[gif source](https://giphy.com/search/prismatic-internet)*

This short seminary with Andrés Colmenares from the internet the founder of  I AM - alternative exploration of internet of culture, relation with an internet how it influence us and the planet was a very nice experience. I discovered a new way and techniques about of to present the concept of the future from the culture of internet perspective.

—> 3 pillars

- critical optimism

- planetary narratives

- long-term strategies

**What if we go Beyond Borders?**

['e-v-e-r-y-t-h-i-n-g']([https://medium.com/iam-journal/beyond-borders-the-e-v-e-r-y-t-h-i-n-g-manifesto-proposal-version-7f6f94e15f2c](https://medium.com/iam-journal/beyond-borders-the-e-v-e-r-y-t-h-i-n-g-manifesto-proposal-version-7f6f94e15f2c)) - manifesto is questioning our understanding of identity, complexity of the systems, design as a tool for promoting coexistence, speed and growth, long term thinking, black mirror, embodied experience, ethical implication of the technology, collectiveness, selfishness of human-centered-design, design education.

After the reading this article I was questioning myself:

- What is our role as a designers in a digital future society?

Not only I asked myself this question.  Also D.Rams did it: *Is my design a good design?* and also Mr and Mrs Eames: *What are the boundaries of design? What are the boundaries of problems?* It does no matter what times we live. What is important for me nowadays in 2019 is popular motto because of the climate change "Act local think Global".*

- What is our mission?

In the society that is based on plurality among others human experience, machine learning, internet of things. Where in this post-technological era feelings are still something unpredictable. The purpose of this question is not find the correct answer but give a space for conversation and try to generate new questions based on the thoughts that came to our head. It is a tool to understand the world better.

I from the internet user to the internet citizens (shifting from internet users to internet citizen that have rights and duties). In 2050 I am going to change my position on the planet as a part of the internet (planetary) citizenship. It is interesting what will be a position of learning  in the future? How the media /digital /tech literacy, cultural innovation are going to look like? What kind of tools are we going to use? The collectiveness foster our creativity!

**IAM WTF! principles:**

- futures always in plural

- futures as a tool, not as a destination

- futures to  be invented, are not predicted

You can have a look on the result of my group work [here](https://drive.google.com/open?id=1LeSsx6QhpbH6Ew81-fw-Opbq4Wl_JeGD).
